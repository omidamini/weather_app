import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState}from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import * as Location from 'expo-location';

import DateTime from './components/DateTime'
import WeatherScroll from './components/WeatherScroll'
const API_KEY ='bc79430dacf8fd0bba0ef4eb8549cca5';
const imgBackground = require('./assets/lyon-1.jpg')
export default function App() {
  const [data, setData] = useState({});
  const [citydata, setcitydata] = useState({});
  const [description, setDescription] = useState();
  const [weatherIcon, setWeatherIcon] = useState();

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        fetchDataFromApi("45.763420", "4.834277")
        return;
      }
      let location = await Location.getCurrentPositionAsync({});
      fetchDataFromApi(location.coords.latitude, location.coords.longitude);
    })();
  }, [])
  
  const fetchDataFromApi = (latitude, longitude) => {
    if(latitude && longitude) {
      fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=hourly,minutely&units=metric&appid=${API_KEY}&lang=fr`).then(res => res.json()).then(data => {
      console.log(data)
      setData(data)
      //console.log(data)
      })
      fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${API_KEY}&lang=fr`).then(res => res.json()).then(citydata => {
      // console.log(data)
      setcitydata(citydata)
      setDescription(citydata.weather[0].description)
      setWeatherIcon(citydata.weather[0].icon)
      console.log(citydata.weather[0].icon)
      })
    }
    
  }
  return (
    <View style={styles.container}>
      <ImageBackground source={imgBackground} style={styles.imageBackground} >
        <DateTime description ={description}  citydata={citydata} lat={data.lat} lon={data.lon} timezone = {data.timezone} weatherIcon = {weatherIcon}/>
        <WeatherScroll weatherData={data.daily}/>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageBackground:{
    flex:1, 
    resizeMode:"cover", 
    justifyContent:"center"
  }
});
