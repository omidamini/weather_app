import React, {useEffect, useState} from 'react'
import {View, Text, StyleSheet, Image} from 'react-native';
import moment from 'moment-timezone'

const days = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi']
const months = ['janv', 'févr', 'mars', 'avril', 'mai', 'juin', 'juil', 'août', 'sept', 'oct', 'nov', 'déc'];

const WeatherItem = ({title, value, unit}) => {
    return(
        <View style={styles.weatherItem}>
            <Text style={styles.weatherItemTitle}>{title}</Text>
            <Text style={styles.weatherItemTitle}>{value}{unit}</Text>
        </View>
    )
}
const WeatherImage = ({icon}) => {
    return(
        <View style={{justifyContent:'center', flexDirection: 'row', textAlign:'center'}}>
            <Image source={{ uri: 'http://openweathermap.org/img/wn/'+ icon +'@4x.png'}} style={styles.image} />
        </View>
    )
}

const DateTime = ({lat, lon, description, citydata, timezone, weatherIcon}) => {
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    
    useEffect (() => {
        setInterval(() => {
            const time = new Date();
            const month = time.getMonth();
            const date = time.getDate();
            const day = time.getDay();
            const hour = time.getHours();
            //const hoursIn12HrFormat = hour >= 13 ? hour %12: hour
            const minutes = time.getMinutes();
            //const ampm = hour >=12 ? 'aprèm': 'matin '
        
            setTime((hour < 10? '0'+hour : hour) + ':' + (minutes < 10? '0'+minutes: minutes) /*+ampm*/) 
            setDate(days[day] + ', ' + date+ ' ' + months[month]) 
        
        }, 1000);
    }, [])
    return (
        <View style={styles.container}>  
           <View>
               <View>
                   <Text style={styles.heading}>{time}</Text>
               </View>
               <View>
                   <Text style={styles.subheading}>{date}</Text>
               </View>
               <View style={styles.weatherItemContainer}>
                    
                    <WeatherImage  icon={weatherIcon?  weatherIcon: ""}/>
                    <Text style={styles.weatherItemTitle}>{description}</Text>
                    <WeatherItem title="Température" value={citydata.main? citydata.main.temp : ""} unit="°c"/>
                    <WeatherItem title="Humidité" value={citydata.main? citydata.main.humidity : ""} unit="%"/>
                    <WeatherItem title="Pression" value={citydata.main? citydata.main.pressure : ""} unit="hPA"/>
                    <WeatherItem title="Lever du soleil" value={citydata.sys? moment.tz(citydata.sys.sunrise * 1000, timezone).format('HH:mm'): ""} unit=" matin"/>
                    <WeatherItem title="Le coucher du soleil " value={citydata.sys? moment.tz(citydata.sys.sunset * 1000, timezone).format('HH:mm') : ""} unit=" soir"/>
               </View>
           </View>
           <View style={styles.rightAlign}>
               <Text style={styles.timezone}>{citydata.name}</Text>
               <Text style={styles.latlong}>{lat}N {lon}E</Text>
           </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1.5,
        flexDirection:"row",
        justifyContent:'space-between',
        padding: 15
    },
    heading: {
        fontSize: 45,
        color:'white',
        fontWeight: '100'
    },
    subheading: {
        fontSize: 25,
        color: '#eee',
        fontWeight: '300'
    },
    rightAlign: {
        textAlign:'right',
        marginTop: 20
    },
    timezone: {
        fontSize: 20,
        color:'white'
    },
    latlong:{
        fontSize:16,
        color:'white',
        fontWeight: '700'
    },
    weatherItemContainer: {
        backgroundColor: "#18181b99",
        borderRadius: 10,
        padding: 10,
        marginTop: 10
    }, 
    weatherItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    weatherItemTitle: {
        color:'#eee',
        fontSize: 14,
        fontWeight: '100'
    },
    image: {
        justifyContent:'center',
        width: 80,
        height: 80
    }
})

export default DateTime
